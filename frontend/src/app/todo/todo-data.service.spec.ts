import { TestBed, inject } from '@angular/core/testing';

import { TodoDataService } from './todo-data.service';
import { Todo } from './todo.model';
import { ApiService } from '../api.service';
import { Observable, of } from 'rxjs';


class ApiMock {

  public getAllTodos(): Observable<Object> {
    return of([{ id: 1, title: 't', description: 'd' }]);
  }

  public createTodo(todo: Todo): Observable<Object> {
    return of({ id: 1, title: todo.title, description: todo.description });
  }

  public updateTodo(todo: Todo): Observable<Object> {
    return of({ id: todo.id, title: todo.title, description: todo.description });
  }

  public deleteTodo(id: number): Observable<Object> {
    return of(null);
  }
}

describe('TodoDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [TodoDataService, { provide: ApiService, useClass: ApiMock }]
  }));

  it('should be injected', inject([TodoDataService], (service: TodoDataService) => {
    expect(service).toBeTruthy();
  }));

  describe('#getAllTodos()', () => {
    it('returns mapped todos', inject([TodoDataService], (service: TodoDataService) => {
      service.getAllTodos().subscribe(
        todos => expect(todos).toEqual([new Todo({ id: 1, title: 't', description: 'd' })])
      )
    }));
  });

  describe('#addTodo(todo)', () => {
    it('returns created todo with assigned id', inject([TodoDataService], (service: TodoDataService) => {
      service.addTodo(new Todo({ title: 'Task 1', description: 'd1' }))
        .subscribe(
          createdTodo => expect(createdTodo).toEqual(new Todo({ id: 1, title: 'Task 1', description: 'd1' }))
        )
    }));
  });

  describe('#deleteTodo(id)', () => {
    it('returns deleted id', inject([TodoDataService], (service: TodoDataService) => {
      service.deleteTodo(7).subscribe(
        deletedId => expect(deletedId).toEqual(7)
      )
    }));
  });

  describe('#updateTodo(todo)', () => {
    it('returns updated todo', inject([TodoDataService], (service: TodoDataService) => {
      service.updateTodo(new Todo({ id: 6, title: 'Task 1', description: 'd1' }))
        .subscribe(
          updatedTodo => expect(updatedTodo).toEqual(new Todo({ id: 6, title: 'Task 1', description: 'd1' }))
        )
    }));
  });
});
