import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TodoEditorComponent } from './todo-editor.component';
import { FormsModule } from '@angular/forms';
import { Component } from '@angular/core';
import { Todo } from '../todo.model';

describe('TodoEditorComponent', () => {
  let component: TodoEditorComponent;
  let fixture: ComponentFixture<TestComponentWrapper>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [TodoEditorComponent, TestComponentWrapper]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestComponentWrapper);
    component = fixture.debugElement.children[0].componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

@Component({
  selector: `host-component`,
  template: `<app-todo-editor [todo]="todo"></app-todo-editor>`
})
class TestComponentWrapper {
  todo = new Todo();
}
