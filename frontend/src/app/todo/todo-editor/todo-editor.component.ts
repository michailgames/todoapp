import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Todo } from '../todo.model';

@Component({
  selector: 'app-todo-editor',
  templateUrl: './todo-editor.component.html',
  styleUrls: ['./todo-editor.component.css']
})

export class TodoEditorComponent {

  @Input() titleText: string;
  @Input() saveText: string;
  @Input() todo: Todo;;
  @Output() save: EventEmitter<Todo> = new EventEmitter();
  @Output() close: EventEmitter<void> = new EventEmitter();

  private onSave() {
    this.save.emit(this.todo);
  }

  onClose() {
    this.close.emit();
  }
}
