import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Todo } from '../todo.model';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent {

  @Input() todos: Todo;
  @Output() delete: EventEmitter<Todo> = new EventEmitter();
  @Output() update: EventEmitter<Todo> = new EventEmitter();

  private onDelete(todo: Todo) {
    this.delete.emit(todo);
  }

  private onUpdate(todo: Todo) {
    this.update.emit(todo);
  }
}
