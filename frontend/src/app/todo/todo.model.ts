export class Todo {
    id: number;
    title: string;
    description: string;

    constructor(values: Object = {}) {
        Object.assign(this, values);
    };
}
