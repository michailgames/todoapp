import { Injectable } from '@angular/core';
import { Todo } from './todo.model';
import { ApiService } from '../api.service';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TodoDataService {

  constructor(private api: ApiService) { }

  public getAllTodos(): Observable<Todo[]> {
    return this.api
      .getAllTodos()
      .pipe(map((response: any[]) => response.map(todo => new Todo(todo)))
        , catchError(this.handleError));
  }

  public addTodo(todo: Todo): Observable<Todo> {
    return this.api
      .createTodo(todo)
      .pipe(map((response: any) => new Todo(response))
        , catchError(this.handleError));
  }

  public deleteTodo(id: number): Observable<number> {
    return this.api
      .deleteTodo(id)
      .pipe(map((response: any) => id)
        , catchError(this.handleError));
  }

  public updateTodo(todo: Todo): Observable<Todo> {
    return this.api
      .updateTodo(todo)
      .pipe(map((response: any) => new Todo(response))
        , catchError(this.handleError));
  }

  private handleError(error: Response | any) {
    console.error('ApiService::handleError', error);
    return throwError(error);
  }
}
