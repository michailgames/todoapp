import { Component, Output, EventEmitter } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Todo } from '../todo.model';

@Component({
  selector: 'app-todo-list-header',
  templateUrl: './todo-list-header.component.html',
  styleUrls: ['./todo-list-header.component.css']
})
export class TodoListHeaderComponent {

  @Output() create: EventEmitter<Todo> = new EventEmitter();

  constructor(private modalService: NgbModal) { }

  private newTodo: Todo = new Todo;
  private modal: NgbModalRef<any>;

  private openEditDialog(content: any) {
    this.newTodo = new Todo();
    this.modal = this.modalService.open(content);
  }

  private onSave(todo: Todo) {
    this.modal.close();
    this.create.emit(todo);
  }

  private modalClose() {
    this.modal.close();
  }
}
