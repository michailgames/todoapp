import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Todo } from '../todo.model'
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-todo-list-item',
  templateUrl: './todo-list-item.component.html',
  styleUrls: ['./todo-list-item.component.css']
})
export class TodoListItemComponent {

  @Input() model: Todo;
  @Output() delete: EventEmitter<Todo> = new EventEmitter();
  @Output() update: EventEmitter<Todo> = new EventEmitter();
  private modal: NgbModalRef<any>;
  private editModel: Todo;

  constructor(private modalService: NgbModal) { }

  private onDelete(): void {
    this.delete.emit(this.model);
  }

  private openEditDialog(content: any) {
    this.editModel = Object.assign({}, this.model);
    this.modal = this.modalService.open(content);
  }

  private onUpdate(todo: Todo): void {
    this.modal.close();
    this.update.emit(todo);
  }

  private modalClose(): void {
    this.modal.close();
  }
}
