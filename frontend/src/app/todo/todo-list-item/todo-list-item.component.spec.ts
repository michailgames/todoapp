import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TodoListItemComponent } from './todo-list-item.component';
import { Todo } from '../todo.model';
import { Component } from '@angular/core';
import { TodoEditorComponent } from '../todo-editor/todo-editor.component';
import { FormsModule } from '@angular/forms';

describe('TodoListItemComponent', () => {
  let component: TodoListItemComponent;
  let fixture: ComponentFixture<TestComponentWrapper>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [TodoListItemComponent, TestComponentWrapper, TodoEditorComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestComponentWrapper);
    component = fixture.debugElement.children[0].componentInstance;
    fixture.detectChanges();
  });

  it('should create with non-null model', () => {
    expect(component).toBeTruthy();
  });
});

@Component({
  selector: `host-component`,
  template: `<app-todo-list-item [model]="todo"></app-todo-list-item>`
})
class TestComponentWrapper {
  todo = new Todo();
}
