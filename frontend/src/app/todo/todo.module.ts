import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TodoListComponent } from './todo-list/todo-list.component';
import { TodoListItemComponent } from './todo-list-item/todo-list-item.component';
import { TodoListHeaderComponent } from './todo-list-header/todo-list-header.component';
import { FormsModule } from '@angular/forms';
import { TodoEditorComponent } from './todo-editor/todo-editor.component';


@NgModule({
  declarations: [TodoListComponent, TodoListItemComponent, TodoListHeaderComponent, TodoEditorComponent],
  imports: [
    CommonModule,
    FormsModule,
  ],
  exports: [TodoListComponent, TodoListHeaderComponent],
})
export class TodoModule { }
