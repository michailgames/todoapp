import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { ApiService } from './api.service';
import { HttpEvent, HttpEventType } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Todo } from './todo/todo.model';

const API_URL = environment.apiUrl;

describe('ApiService', () => {
  let service: ApiService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ApiService]
    });
    service = TestBed.get(ApiService);
    httpMock = TestBed.get(HttpTestingController);
  });

  describe('tasks endpoint', () => {
    it('performs GET on getAllTodos()', () => {
      let mockResponse = [
        { id: 1, title: 'title1', description: "description1" },
        { id: 1, title: 'title1', description: "description2" }
      ];
      service.getAllTodos().subscribe((event: HttpEvent<any>) => {
        switch (event.type) {
          case HttpEventType.Response:
            expect(event.body).toEqual(mockResponse);
        }
      });
      let request = httpMock.expectOne(API_URL + "/tasks");
      expect(request.request.method).toBe('GET')
      expect(request.request.responseType).toEqual('json');
      request.flush(mockResponse);
      httpMock.verify();
    });

    it('performs POST on createTodo()', () => {
      let mockResponse = { id: 17, title: 'title', description: "description" };
      service.createTodo(new Todo({ title: 'title', description: 'description' }))
        .subscribe((event: HttpEvent<any>) => {
          switch (event.type) {
            case HttpEventType.Response:
              expect(event.body).toEqual(mockResponse);
          }
        });
      let request = httpMock.expectOne(API_URL + "/tasks");
      expect(request.request.method).toBe('POST')
      expect(request.request.responseType).toEqual('json');
      request.flush(mockResponse);
      httpMock.verify();
    });

    it('performs PUT on updateTodo()', () => {
      let mockResponse = { id: 17, title: 'title', description: "description" };
      service.updateTodo(new Todo({ id: 17, title: 'title', description: 'description' }))
        .subscribe((event: HttpEvent<any>) => {
          switch (event.type) {
            case HttpEventType.Response:
              expect(event.body).toEqual(mockResponse);
          }
        });
      let request = httpMock.expectOne(API_URL + "/tasks/17");
      expect(request.request.method).toBe('PUT')
      expect(request.request.responseType).toEqual('json');
      request.flush(mockResponse);
      httpMock.verify();
    });

    it('performs DELETE on deleteTodo()', () => {
      service.deleteTodo(10)
        .subscribe((event: HttpEvent<any>) => {
          switch (event.type) {
            case HttpEventType.Response:
              expect(event.body).toEqual({});
          }
        });
      let request = httpMock.expectOne(API_URL + "/tasks/10");
      expect(request.request.method).toBe('DELETE')
      expect(request.request.responseType).toEqual('json');
      request.flush({});
      httpMock.verify();
    });
  });
});
