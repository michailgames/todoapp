import { Component, OnInit } from '@angular/core';
import { TodoDataService } from './todo/todo-data.service';
import { Todo } from './todo/todo.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [TodoDataService],
})
export class AppComponent implements OnInit {

  private todoDataService: TodoDataService;
  private todos: Todo[] = [];

  constructor(todoDataService: TodoDataService) {
    this.todoDataService = todoDataService;
  }

  public ngOnInit() {
    this.todoDataService
      .getAllTodos()
      .subscribe(
        (todos) => {
          this.todos = todos;
        }
      );
  }

  private getAllTodos(): Todo[] {
    return this.todos;
  }

  private deleteTodo(todo: Todo): void {
    this.todoDataService
      .deleteTodo(todo.id)
      .subscribe(
        (_) => {
          this.todos = this.todos.filter(t => t.id != todo.id);
        }
      );
  }

  private updateTodo(todo: Todo): void {
    this.todoDataService
      .updateTodo(todo)
      .subscribe(
        (updatedTodo) => {
          this.todos = this.todos.map(t => t.id == updatedTodo.id ? updatedTodo : t);
        }
      );
  }

  private createTodo(todo: Todo): void {
    this.todoDataService
      .addTodo(todo)
      .subscribe(
        (newTodo) => {
          this.todos = this.todos.concat(newTodo);
        }
      );
  }
}
