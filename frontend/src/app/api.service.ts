import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable, pipe, throwError } from 'rxjs';
import { Todo } from './todo/todo.model';

const API_URL = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  public getAllTodos(): Observable<Object> {
    return this.http.get(API_URL + '/tasks');
  }

  public createTodo(todo: Todo): Observable<Object> {
    return this.http.post(API_URL + '/tasks', todo);
  }

  public updateTodo(todo: Todo): Observable<Object> {
    return this.http.put(API_URL + '/tasks/' + todo.id, todo);

  }

  public deleteTodo(id: number): Observable<Object> {
    return this.http.delete(API_URL + '/tasks/' + id);
  }
}
