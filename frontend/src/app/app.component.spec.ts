import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { Component, Input } from '@angular/core';
import { LayoutComponent } from './layout/layout/layout.component';
import { Todo } from './todo/todo.model';
import { HeaderComponent } from './layout/header/header.component';
import { TodoDataService } from './todo/todo-data.service';
import { ApiService } from './api.service';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      providers: [
        { provide: TodoDataService, useClass: TodoDataServiceMock },
        { provide: ApiService, useClass: ApiServiceMock },
      ],
      declarations: [
        AppComponent, LayoutComponent, MockTodoList, HeaderComponent, MockTodoListHeader,
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });
});

@Component({
  selector: 'app-todo-list',
  template: ''
})
class MockTodoList {
  @Input() todos: Todo;
}

@Component({
  selector: 'app-todo-list-header',
  template: ''
})
class MockTodoListHeader {
  @Input() todos: Todo;
}

class TodoDataServiceMock { }
class ApiServiceMock { }
