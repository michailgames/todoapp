import { browser, by, element, ElementArrayFinder, ElementFinder } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get(browser.baseUrl) as Promise<any>;
  }

  getNavbar() {
    return element(by.css('app-root app-header nav')) as ElementFinder;
  }

  getTaskListHeaderText() {
    return element(by.css('app-root .container h1')).getText() as Promise<string>;
  }

  getNewTaskButton() {
    return element(by.css('app-root app-todo-list-header button')) as ElementFinder;
  }

  getTaskEditorDialog() {
    return element(by.css('body ngb-modal-window app-todo-editor')) as ElementFinder;
  }

  getTodoItems() {
    return element.all(by.css('app-root .container app-todo-list app-todo-list-item')) as ElementArrayFinder;
  }
}
