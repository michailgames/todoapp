import { AppPage } from './app.po';
import { browser, logging, by, ElementArrayFinder, ElementFinder, ProtractorBy } from 'protractor';
import { ProtractorLocator, Locator } from 'protractor/built/locators';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
    page.navigateTo();
    let todos = page.getTodoItems();
    todos.each(todo => {
      todo.element(by.buttonText('Delete')).click();
    })
    expect(todos.count()).toBe(0);
  });

  it('should display navbar', () => {
    expect(page.getNavbar()).toBeTruthy();
  });

  it('should display empty task list on start', () => {
    expect(page.getTaskListHeaderText()).toEqual('Your tasks');
    expect(page.getTodoItems().count()).toBe(0);
  });

  function editTaskDetails(title: string, description: string, confirmButton: Locator) {
    let editorDialog = page.getTaskEditorDialog();
    let textFields = editorDialog.all(by.css('input'));
    textFields.get(0).clear();
    textFields.get(0).sendKeys(title);
    textFields.get(1).clear();
    textFields.get(1).sendKeys(description);
    editorDialog.element(confirmButton).click();
  }

  it('should add task afer clicking add and filling details', () => {
    page.getNewTaskButton().click();
    editTaskDetails('task1', 'some description', by.buttonText('Create'));
    let todos = page.getTodoItems();
    expect(todos.count()).toBe(1);
    expect(todos.get(0).element(by.css('h5')).getText()).toBe('task1');
    expect(todos.get(0).element(by.css('p')).getText()).toBe('some description');
  });

  it('should remove task after clicking delete on task', () => {
    page.getNewTaskButton().click();
    editTaskDetails('task1', 'some description', by.buttonText('Create'));
    let todos = page.getTodoItems();
    expect(todos.count()).toBe(1);
    todos.get(0).element(by.buttonText('Delete')).click();
    expect(todos.count()).toBe(0);
  });

  it('should update task after clicking edit and changing details', () => {
    page.getNewTaskButton().click();
    editTaskDetails('task1', 'some description', by.buttonText('Create'));
    let todos = page.getTodoItems();
    expect(todos.count()).toBe(1);
    todos.get(0).element(by.buttonText('Edit')).click();
    editTaskDetails('Task1', 'better description', by.buttonText('Save'));
    expect(todos.count()).toBe(1);
    expect(todos.get(0).element(by.css('h5')).getText()).toBe('Task1');
    expect(todos.get(0).element(by.css('p')).getText()).toBe('better description');
  });

  it('should not add task when new task dialog is closed', () => {
    page.getNewTaskButton().click();
    editTaskDetails('task1', 'some description', by.css('.modal-header button'));
    let todos = page.getTodoItems();
    expect(todos.count()).toBe(0);
  });

  it('should not add task when edit dialog is closed', () => {
    page.getNewTaskButton().click();
    editTaskDetails('old title', 'old description', by.buttonText('Create'));
    let todos = page.getTodoItems();
    expect(todos.count()).toBe(1);
    todos.get(0).element(by.buttonText('Edit')).click();
    editTaskDetails('new title', 'new description', by.css('.modal-header button'));
    expect(todos.count()).toBe(1);
    expect(todos.get(0).element(by.css('h5')).getText()).toBe('old title');
    expect(todos.get(0).element(by.css('p')).getText()).toBe('old description');
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});

