package com.michailgames.todoApp.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static java.util.stream.Collectors.toList;

@Service
public class TaskService {

    private final TaskRepository taskRepository;

    @Autowired
    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    List<TaskDTO> getAllTasks() {
        Iterable<Task> tasks = taskRepository.findAll();
        return StreamSupport.stream(tasks.spliterator(), false)
                .map(TaskDTO::new)
                .collect(toList());
    }

    Optional<TaskDTO> findTask(long id) {
        return taskRepository.findById(id).map(TaskDTO::new);
    }

    void deleteTask(long id) throws TaskNotFoundException {
        if (taskRepository.existsById(id)) {
            taskRepository.deleteById(id);
        } else throw new TaskNotFoundException();
    }

    TaskDTO save(TaskDTO task) {
        Task saved = taskRepository.save(new Task(null, task.getTitle(), task.getDescription()));
        return new TaskDTO(saved);
    }

    TaskDTO updateTask(TaskDTO task, long id) throws TaskNotFoundException {
        if (taskRepository.existsById(id)) {
            Task saved = taskRepository.save(new Task(id, task.getTitle(), task.getDescription()));
            return new TaskDTO(saved);
        } else throw new TaskNotFoundException();
    }

    static class TaskNotFoundException extends Exception {

    }
}