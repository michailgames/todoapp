package com.michailgames.todoApp.task;

public class TaskDTO {

    private final Long id;
    private final String title;
    private final String description;

    public TaskDTO(Task task) {
        this(task.getId(), task.getTitle(), task.getDescription());
    }

    public TaskDTO(Long id, String title, String description) {
        this.id = id;
        this.title = title;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }
}
