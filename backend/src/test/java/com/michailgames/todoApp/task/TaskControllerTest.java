package com.michailgames.todoApp.task;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class TaskControllerTest {

    @Mock
    private TaskRepository taskRepository;
    private TaskController taskController;

    @BeforeEach
    void setup() {
        taskController = new TaskController(new TaskService(taskRepository));
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
    }

    @Test
    void getAllTasksReturnsEmptyListWhenThereAreNoTasks() {
        when(taskRepository.findAll()).thenReturn(new ArrayList<>());
        assertThat(taskController.getAllTasks()).isEmpty();
    }


    @Test
    void getAllTasksReturnsEmptyAllExistingTasks() {
        when(taskRepository.findAll()).thenReturn(Arrays.asList(task(1), task(2)));
        assertThat(ids(taskController.getAllTasks())).containsExactly(1L, 2L);
    }

    @Test
    void getTaskReturnsExistingTaskById() {
        when(taskRepository.findById(2L)).thenReturn(Optional.of(new Task(2L, "title", "description")));
        ResponseEntity<TaskDTO> task = taskController.getTask(2);
        verifyTask(requireNonNull(task.getBody()), 2L, "title", "description");
    }

    @Test
    void getTaskReturnsNotFoundWhenTaskDoesNotExist() {
        when(taskRepository.findById(1L)).thenReturn(Optional.empty());
        ResponseEntity<TaskDTO> task = taskController.getTask(1);
        assertThat(task.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    void createSavesNewTask() {
        TaskDTO task = new TaskDTO(null, "aaa", "bbb");
        when(taskRepository.save(any())).thenReturn(new Task(1L, "aaa", "bbb"));
        ResponseEntity<TaskDTO> response = taskController.createTask(task);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        verifyTask(requireNonNull(response.getBody()), 1L, "aaa", "bbb");
        verify(taskRepository).save(argThat(t -> "aaa".equals(t.getTitle())
                && "bbb".equals(t.getDescription())));
    }

    @Test
    void deleteRemovesExistingTask() {
        when(taskRepository.existsById(1L)).thenReturn(true);
        ResponseEntity<Task> response = taskController.deleteTask(1L);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
        verify(taskRepository).deleteById(1L);
    }

    @Test
    void deleteReturnsNotFoundWhenTaskDoesNotExist() {
        when(taskRepository.existsById(1L)).thenReturn(false);
        ResponseEntity<Task> response = taskController.deleteTask(1L);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    void updateUpdatesExistingTask() {
        when(taskRepository.existsById(1L)).thenReturn(true);
        TaskDTO task = new TaskDTO(null, "aaa", "someDescription");
        when(taskRepository.save(any())).thenReturn(new Task(1L, "aaa", "someDescription"));
        ResponseEntity<TaskDTO> response = taskController.updateTask(task, 1L);
        verifyTask(requireNonNull(response.getBody()), 1L, "aaa", "someDescription");
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        verify(taskRepository).save(argThat(t -> t.getId() == 1
                && "aaa".equals(t.getTitle()) && "someDescription".equals(t.getDescription())));
    }

    @Test
    void updateReturnsNotFoundWhenTaskDoesNotExist() {
        when(taskRepository.existsById(1L)).thenReturn(false);
        ResponseEntity<TaskDTO> response = taskController.updateTask(new TaskDTO(null, "aa", "bb"), 1L);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    private List<Long> ids(List<TaskDTO> tasks) {
        return tasks.stream().map(TaskDTO::getId).collect(toList());
    }

    private Task task(long id) {
        return new Task(id, "", "");
    }

    private void verifyTask(TaskDTO task, long id, String title, String description) {
        assertThat(task.getId()).isEqualTo(id);
        assertThat(task.getTitle()).isEqualTo(title);
        assertThat(task.getDescription()).isEqualTo(description);
    }
}
